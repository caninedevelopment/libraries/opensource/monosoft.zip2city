namespace Monosoft.Zip2City.Persistence.PostgreSQL
{
    using Monosoft.Zip2City.Domain.Interfaces;
    using global::Monosoft.Zip2City.Persistence.PostgreSQL.Repositories;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependencyInjection
    {
        public static IServiceCollection AddPostgreSQLPersistenceProvider(this IServiceCollection services, string dbServerHost, string dbUser, string dbPassword, int dbServerPort, string dbName)
        {
            services.AddDbContext<Persistence.PostgreSQL.DbContext>(options => options.UseNpgsql($"Host={dbServerHost};User ID={dbUser};Password={dbPassword};Port={dbServerPort};Database={dbName};"));

            services.AddSingleton<IZipCodeRepository, ZipCodeRepository>();

            return services;
        }
    }
}