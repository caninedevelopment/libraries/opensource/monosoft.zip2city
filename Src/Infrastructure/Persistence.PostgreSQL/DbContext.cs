﻿namespace Monosoft.Zip2City.Persistence.PostgreSQL
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;

    public class DbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbContext(DbContextOptions<DbContext> options)
            : base(options)
        {
            this.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DbContext).Assembly);
        }

    }
}
