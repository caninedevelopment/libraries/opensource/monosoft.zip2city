﻿using System;
using System.Collections.Generic;
using System.Text;
using CanineDotNet.Zip2City.Persistence.MongoDB;
using Microsoft.Extensions.DependencyInjection;
using Monosoft.Zip2City.Domain.Interfaces;
using Persistence.MongoDB.Repositories;

namespace Persistence.MongoDB
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistenceProvider(this IServiceCollection services, string dbServerHost, string dbUser, string dbPassword, int dbServerPort, string dbName)
        {
            var context = new DbContext(dbUser, dbPassword, dbServerHost, dbServerPort, dbName);
            services.AddSingleton<DbContext>(context);

            services.AddSingleton<IZipCodeRepository, ZipCodeRepository>();

            return services;
        }
    }
}