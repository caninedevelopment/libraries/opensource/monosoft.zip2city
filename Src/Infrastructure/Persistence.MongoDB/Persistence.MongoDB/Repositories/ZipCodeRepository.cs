﻿using CanineDotNet.Zip2City.Persistence.MongoDB;
using MongoDB.Bson;
using MongoDB.Driver;
using Monosoft.Zip2City.Domain.Entities;
using Monosoft.Zip2City.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.MongoDB.Repositories
{
    public class ZipCodeRepository : IZipCodeRepository
    {

        private readonly DbContext context;

        public ZipCodeRepository(DbContext context)
        {
            this.context = context;
        }
        const string collectionName = "zipcode";

        private IMongoCollection<ZipCodeInfo> collection
        {
            get { return this.context.db.GetCollection<ZipCodeInfo>(collectionName); }
        }

        public void Delete(string country, string zipcode)
        {
            var builder = Builders<ZipCodeInfo>.Filter;
            var filter1 = builder.Eq(x => x.Country, country);
            var filter2 = builder.Eq(x => x.ZipCode, zipcode);
            collection.DeleteOne(filter1 & filter2);
        }

        public ZipCodeInfo GetByCountryAndZipCode(string country, string zipcode)
        {
            var builder = Builders<ZipCodeInfo>.Filter;
            var filter1 = builder.Eq(x => x.Country, country);
            var filter2 = builder.Eq(x => x.ZipCode, zipcode);
            return collection.Find(filter1 & filter2).FirstOrDefault();
        }

        public void Insert(ZipCodeInfo zipCodeInfo)
        {
            collection.InsertOne(zipCodeInfo);
        }

        public void Update(ZipCodeInfo zipCodeInfo)
        {
            var builder = Builders<ZipCodeInfo>.Filter;
            var filter1 = builder.Eq(x => x.Country, zipCodeInfo.Country);
            var filter2 = builder.Eq(x => x.ZipCode, zipCodeInfo.ZipCode);
            collection.ReplaceOne(filter1 & filter2, zipCodeInfo);
        }
    }
}
