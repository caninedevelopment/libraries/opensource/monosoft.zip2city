﻿using CanineDotNet.Common.Exceptions;
using CanineDotNet.Common.Interfaces.Command;
using Monosoft.Zip2City.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Zip2City.Application.Resources.ZipCode.Commands.Update
{
    public class Command : IProcedure<Request>
    {
        private readonly IZipCodeRepository _zipCodeRepository;

        public Command(IZipCodeRepository zipCodeRepository)
        {
            this._zipCodeRepository = zipCodeRepository;
        }

        public void Execute(Request input)
        {
            var zipcode = _zipCodeRepository.GetByCountryAndZipCode(input.Country, input.ZipCode);
            if (zipcode.ZipCode == input.ZipCode && zipcode.Country == input.Country)
            {
                zipcode = new Domain.Entities.ZipCodeInfo()
                {
                    Country = input.Country,
                    ZipCode = input.ZipCode,
                    CityName = input.CityName,
                    ZipCodeRangeStart = input.ZipCodeRangeStart,
                };

                _zipCodeRepository.Insert(zipcode);
            }
            else
            {
                throw new ElementDoesNotExistException("Zipcode dons not exist", input.ZipCode);
            }
        }
    }
}
