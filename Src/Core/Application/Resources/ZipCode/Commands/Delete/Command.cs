﻿using CanineDotNet.Common.Interfaces.Command;
using Monosoft.Zip2City.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Zip2City.Application.Resources.ZipCode.Commands.Delete
{
    public class Command : IProcedure<Request>
    {
        private readonly IZipCodeRepository _zipCodeRepository;

        public Command(IZipCodeRepository zipCodeRepository)
        {
            this._zipCodeRepository = zipCodeRepository;
        }

        public void Execute(Request input)
        {
            _zipCodeRepository.Delete(input.Country, input.ZipCode);
        }
    }
}
