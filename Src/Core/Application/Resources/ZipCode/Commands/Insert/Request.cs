﻿using CanineDotNet.Common.Interfaces.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Zip2City.Application.Resources.ZipCode.Commands.Insert
{
    public class Request : IDtoRequest
    {
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string CityName { get; set; }
        public string ZipCodeRangeStart { get; set; }

        public void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
