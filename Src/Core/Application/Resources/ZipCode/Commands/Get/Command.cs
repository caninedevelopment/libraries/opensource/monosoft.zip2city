namespace Monosoft.Zip2City.Application.Resources.ZipCode.Commands.Get
{
    using Monosoft.Zip2City.Domain.Interfaces;
    using CanineDotNet.Common.Interfaces.Command;

    public class Command : IFunction<Request, Response>
    {
        private readonly IZipCodeRepository zipRepo;

        public Command(IZipCodeRepository zipRepo)
        {
            this.zipRepo = zipRepo;
        }

        public Response Execute(Request input)
        {
            var zipcode = zipRepo.GetByCountryAndZipCode(input.Country, input.ZipCode);

            return new Response(zipcode);
        }
    }
}