using CanineDotNet.Common.Exceptions;
using CanineDotNet.Common.Interfaces.Command;

namespace Monosoft.Zip2City.Application.Resources.ZipCode.Commands.Get
{

    public class Request : IDtoRequest
    {
        public string Country { get; set; }

        public string ZipCode { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(this.Country))
            {
                throw new ValidationException(nameof(Country));
            }

            if (string.IsNullOrEmpty(this.ZipCode))
            {
                throw new ValidationException(nameof(ZipCode));
            }
        }
    }
}