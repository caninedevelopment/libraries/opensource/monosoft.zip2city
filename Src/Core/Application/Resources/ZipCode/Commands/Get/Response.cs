namespace Monosoft.Zip2City.Application.Resources.ZipCode.Commands.Get
{
    using CanineDotNet.Common.Interfaces.Command;
    using Monosoft.Zip2City.Domain.Entities;

    public class Response : IDtoResponse
    {
        public string CityName { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }

        public Response(ZipCodeInfo zipcode)
        {
            this.CityName = zipcode.CityName;
            this.Country = zipcode.Country;
            this.ZipCode = zipcode.ZipCode;
        }
    }
}