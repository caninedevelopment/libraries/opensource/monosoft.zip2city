﻿namespace Monosoft.Zip2City.Domain.Entities
{
    public class ZipCodeInfo
    {
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string CityName { get; set; }
        public string ZipCodeRangeStart { get; set; }
    }
}
