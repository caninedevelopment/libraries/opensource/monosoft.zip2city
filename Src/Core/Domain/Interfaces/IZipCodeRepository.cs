﻿namespace Monosoft.Zip2City.Domain.Interfaces
{
    using Monosoft.Zip2City.Domain.Entities;

    public interface IZipCodeRepository
    {
        ZipCodeInfo GetByCountryAndZipCode(string country, string zipcode);
        void Update(ZipCodeInfo zipCodeInfo);
        void Insert(ZipCodeInfo zipCodeInfo);
        void Delete(string country, string zipcode);
    }
}
