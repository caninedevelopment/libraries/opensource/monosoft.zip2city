﻿using NUnit.Framework;
using Monosoft.Zip2City.Persistence.PostgreSQL.Repositories;
using CanineDotNet.Common.Exceptions;

namespace Persistence.PostgreSQL.UnitTest.Repositories
{
    [TestFixture]
    public class GetByCountryAndZipCode
    {
        [Test]
        public void Get_Element_Exist()
        {
            ZipCodeRepository repo = new ZipCodeRepository(null);
            var res = repo.GetByCountryAndZipCode("DK", "1503");
            Assert.AreEqual(res.CityName, "København V");
            Assert.AreEqual(res.ZipCode, "1503");
        }

        [Test]
        public void Get_Element_Does_Not_Exist_Exception()
        {
            ZipCodeRepository repo = new ZipCodeRepository(null);
            Assert.Throws<ElementDoesNotExistException>(() => repo.GetByCountryAndZipCode("DK", "800"));
        }
    }
}
